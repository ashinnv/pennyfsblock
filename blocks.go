package blocks

import (
	"fmt"
	"os"

	"../pennyfsconfig"

	"crypto/sha256"
	"io/ioutil"
	"strings"
	"time"
)

type PConf pennyfsconfig.PConf

type ModDiffs struct {
	Uname   string       //Who modified this?
	ModTime time.Time    //When modified this?
	DiffDat map[int]byte //Index along file data where the change occurred and the byte that would be there instead
}

type Block struct {
	FileDat    []byte                 //Actual file data
	FilePath   string                 //File's filepath and name
	FileHash   []byte                 //SHA256 hash of file
	ColorRange map[int]float32        //Histogram for image types. Int is hue from red to violet and float is the value amount
	AddedDate  time.Time              //Time file was added
	ModDiffs   map[time.Time]ModDiffs //Times that this block was modified and the username who did so
	ClassTags  []string               //Classifier tags for classifiers such as image classification or audio classification
	UClassTags []string               //User-submitted classification tags such as "my favorite" or "why is this picture of her on your computer?"
}

/*
	GenBlock is given a directory location (full path) of a file and generates a block
	structure out of it. This is then returned along with an error. The original file either
	gets left alone, deleted, or moved to a trash folder, as per user configuration.
*/
func GenBlock(dirLoc string, cfg *PConf) (error, Block) {

	var blkBlk Block

	fileBytes, err := ioutil.ReadFile(dirLoc)
	if err != nil {
		return err, blkBlk
	}

	diffMapDed := make(map[time.Time]ModDiffs)
	nameSplit := strings.Split(dirLoc, ".")
	slshSplit := strings.Split(dirLoc, "/")
	fileName := slshSplit[len(slshSplit)]
	fileExt := nameSplit[len(nameSplit)]

	_, hist := checkGenHistogram(fileExt, fileBytes) // Function is currently spiked

	var retBlk Block = Block{
		fileBytes,
		fileName,
		genSHA256(fileBytes),
		hist,
		time.Now(),
		diffMapDed,
		[]string{""},
		[]string{""},
	}

	if cfg.KeepFileMode == 0 {
		fderr := os.Remove(dirLoc)
		if err != nil {
			fmt.Println("Error deleting file:", fderr)
		}
	} else if cfg.KeepFileMode == 1 {
		mverr := os.Rename(dirLoc, cfg.TrashLoc+"/"+fileName+"."+fileExt)
		if mverr != nil {
			fmt.Println("Error moving file:", mverr)
		}
	}

	return nil, retBlk
}

//If the incoming data is an image, return true and a histogram of that image. Else, return false and a blank histogram
func checkGenHistogram(fileExt string, fDat []byte) (bool, map[int]float32) {

	/*
		The histogram is a map of integer indexes, with the int representing a certain color value
		and a float32 value, representing the amount of the image that is that color.

	*/

	var mtMap = make(map[int]float32)
	return false, mtMap

	/*

		fileExts := []string{"jpg", "jpeg", "png", "gif", "tiff", "tif", "bmp", "dib", "webp", "psd", "raw", "arw", "cr2", "nrw", "k25", "heif", "heic", "indd", "indd", "indt", "jp2", "j2k", "jpf", "jpx", "jpm", "mj2", "svg", "svgz", "ai", "eps"}
		isImg := false
		for _, ex := range fileExts {
			if fileExt == ex {
				isImg = true
				break
			}
		}

		if isImg == false {
			return isImg, mtMap
		}

		var opts jpeg.Options
		opts.Quality = 1

		err = jpeg.Encode
	*/
}

func genSHA256(input []byte) []byte {
	h := sha256.New()
	h.Write(input)
	return h.Sum(nil)
}
